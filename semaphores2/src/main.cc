// SYSTEM INCLUDES
#include <gtest/gtest.h>


// C++ PROJECT INCLUDES
#include "sem/math.h"


TEST(foo, foo_test)
{
    EXPECT_EQ(add_two_ints(2, 4), 6);
}


int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

