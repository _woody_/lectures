#pragma once
#ifndef SEM_QUEUE_H
#define SEM_QUEUE_H

// SYSTEM INCLUDES
#include <queue>
#include <semaphore.h>


// C++ PROJECT INCLUDES


const size_t MAX_QUEUE_SIZE = 64;

template<typename T>
class queue
{
public:
    queue(const T sentinel) : queue(),
        sentinel(sentinel), mutex(),
        full(), empty()
    {
        sem_init(&this->mutex, 0, 1);
        sem_init(&this->full, 0, MAX_QUEUE_SIZE);
        sem_init(&this->empty, 0, 0);
    }

    ~queue()
    {
        sem_destroy(&this->mutex);
        sem_destroy(&this->full);
        sem_destroy(&this->empty);
    }

    // producers will call this member function when they want to add an element to the buffer
    void push(const T& obj)
    {
        sem_wait(&this->full);
        sem_wait(&this->mutex);

        // critical section
        this->queue.push(obj);

        sem_post(&this->mutex);
        sem_post(&this->empty);
    }

    // consumers will call this member function when they want to remove an item from the buffer
    T pop()
    {
        sem_wait(&this->empty);
        sem_wait(&this->mutex);

        // critical section
        T val = queue.front();
        queue.pop();

        sem_post(&this->mutex);
        if(val == this->sentinel) {
            sem_post(&this->empty);
        } else {
            sem_post(&this->full);
        }
    }

private:
    std::queue<T>   queue;          // the actual queue we want to protect
    T               sentinel;       // the stop terminal...note this is not necessary for the
                                    // general solution to the bounded buffer problem
    sem_t           mutex;          // provide mutual exclusion
    sem_t           full;           // the number of free slots
    sem_t           empty;          // the number of items produced
};


#endif
